#!/usr/bin/env python3.2
# DIMM Database Analyzer

import sys
import argparse

import SeeingStats


def process_options():
    parser = argparse.ArgumentParser(description='SeeingStats')

    parser.add_argument('--mass_database', help='Mass Database to use.')
    parser.add_argument('--dimm_database', help='DIMM Database to use.')

    parser.add_argument('--good_seeing', '-gs',
            help='Value for Good Seeing. Default 0.2',
            type=float,
            default=0.2
            )
    parser.add_argument('--bad_seeing', '-bs',
            help='Value for Bad Seeing. Default 1.5',
            type=float,
            default=1.5
            )

    args = parser.parse_args()

    # Prompt for the DBs if we don't have them
    if not args.mass_database:
        db_input = input('Mass DB: ')
        if db_input:
            args.mass_database = db_input
        else:
            print('--mass_database required')
            sys.exit()

    if not args.dimm_database:
        db_input = input('DIMM DB: ')
        if db_input:
            args.dimm_database = db_input
        else:
            print('--dimm_database required')
            sys.exit()

    # Just showing us what was passed
    print('GS: {0:.2f}\nBS: {1:.2f}\nDB:\n\t{2}\n\t{3}'.format(
        args.good_seeing,
        args.bad_seeing,
        args.mass_database,
        args.dimm_database
    ))

    return args


def main():
    args = process_options()

    # Open the DB or exit
    try:
        dimm_db = open(args.dimm_database, 'r')
    except:
        print('Problem reading database: {0}'.format(args.dimm_database))
        sys.exit()

    obs = SeeingStats.WeatherObservations(
            bad_seeing=args.bad_seeing,
            good_seeing=args.good_seeing
    )

    # Add each line from the DB (file)
    for line in dimm_db:
        obs.add(line)

    dimm_db.close()

    obs.stats()
    #obs.graph()
    #obs.csv()

if __name__ == '__main__':
    main()
