import sys
import datetime

from math import isnan

from pylab import figure, savefig
from matplotlib.dates import MONDAY, SATURDAY
from matplotlib.finance import quotes_historical_yahoo
from matplotlib.dates import MonthLocator, WeekdayLocator, DateFormatter


class WeatherObservations:
    """ A class representing a single observation """
    def __init__(self, good_seeing=0.2, bad_seeing=1.5):
        self.good_seeing = good_seeing
        self.bad_seeing = bad_seeing

        # Our main data
        self.mass_list = {}
        self.dimm_list = {}

    def add(self, line):
        """ Adds a line from the observation to our list """

        try:
            # e.g.
            # yyyy mm dd hh mm ss seeing
            # 2009 09 22 07 36 36 0.41370476
            split_line = line.split()
            seeing = float(split_line[6])
            timestamp = datetime.datetime(
                int(split_line[0]),
                int(split_line[1]),
                int(split_line[2]),
                int(split_line[3]),
                int(split_line[4]),
                int(split_line[5])
            )

            self.dimm_list[timestamp] = seeing

        except:
            print('Problem adding line: {0}'.format(split_line))
            print('Error: {0}'.format(sys.exc_info()[0]))

        return

    def __repr__(self):
        return ''

    def csv(self):
        """ Writes our values out to a csv file """
        f = open('dimm.csv', 'w')
        f.write("Date,Seeing\n")
        for dt, seeing in self.dimm_list.items():
            f.write("{0},{1}\n".format(dt, seeing))
        f.close()

    def stats(self):
        """ Show some stats about the observation """
        self.mean()
        self.median()
        self.find_good_seeing()

    def find_good_seeing(self):
        """ Responsible for finding the Good Seeing """

        good_seeing_list = []
        good_seeing_count = 0
        bad_seeing_count = 0

        for seeing in self.dimm_list.values():
            good_seeing_list.append(seeing)
            if seeing <= self.good_seeing and seeing > 0.00:
                good_seeing_count += 1
            elif seeing >= self.bad_seeing:
                bad_seeing_count += 1

        print("{0:30}: {1}".format(
            'Number of obs below seeing',
            good_seeing_count)
            )
        print("{0:30}: {1}".format(
            'Number of obs above seeing',
            bad_seeing_count)
            )

    def mean(self):

        Alist = []
        nanlist = 0
        too_low = 0
        for seeing in self.dimm_list.values():
            if isnan(seeing):
                nanlist += 1
            elif seeing <= 0.00:
                too_low += 1
            else:
                Alist.append(seeing)

        mean = sum(Alist) / len(Alist)

        print("{0:30}: {1:d}".format('Number of NaN obs is', nanlist))
        print("{0:30}: {1:d}".format('Number <= 0.0 obs is', too_low))
        print("{0:30}: {1:d}".format('Number of obs is', len(Alist)))
        print("{0:30}: {1:.2f}".format('Mean', mean))

        return mean

    def median(self):

        num_list = []
        for seeing in self.dimm_list.values():
            if isnan(seeing):
                pass
            else:
                num_list.append(seeing)

        num_list = sorted(num_list)

        if len(num_list) % 2 == 0:
            x = num_list[int(len(num_list) / 2)]
            y = num_list[int(len(num_list) / 2) - 1]
            median = ((x + y) / 2)
        else:
            median = (num_list[int(len(num_list) / 2)])

        print("{0:30}: {1}".format('Median', median))

        return median

    def graph(self):

        # every monday
        mondays = WeekdayLocator(MONDAY)

        # every 3rd month
        months = MonthLocator(range(1, 13), bymonthday=1, interval=3)
        monthsFmt = DateFormatter("%b '%y")

        dates = []
        opens = []

        for dt in sorted(self.dimm_list.keys()):
            dates.append(dt)
            opens.append(self.dimm_list[dt])

        fig = figure()
        ax = fig.add_subplot(111)
        ax.plot_date(dates, opens, '-')
        ax.xaxis.set_major_locator(months)
        ax.xaxis.set_major_formatter(monthsFmt)
        ax.xaxis.set_minor_locator(mondays)
        ax.autoscale_view()
        ax.xaxis.grid(False, 'major')
        ax.xaxis.grid(True, 'minor')
        ax.grid(True)

        fig.autofmt_xdate()

        savefig('{0}.png'.format(
            datetime.datetime.today().strftime('%F-%H-%M-%S')
        ))
